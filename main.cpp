/*
 * Мишуткин Александр, БПИ-192
 * Вариант-16
 * 10.12.2020
 *
 * На клумбе растет 40 цветов, за ними непрерывно
 * следят два садовника и поливают увядшие цветы, при этом оба садовника
 * очень боятся полить один и тот же цветок. Создать многопоточное
 * приложение, моделирующее состояния клумбы и действия садовников. Для
 * изменения состояния цветов создать отдельный поток.
 */

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma ide diagnostic ignored "performance-unnecessary-value-param"

#include <iostream>
#include <thread>
#include <vector>
#include <memory>
#include <unordered_map>
#include <sstream>
#include <ctime>
#include <string>
#include <random>
#include <mutex>
#include <chrono>

using namespace std::chrono_literals;

auto duration = 100ms; // NOLINT(cert-err58-cpp)

std::mt19937 gen(std::chrono::duration_cast<std::chrono::nanoseconds>( // NOLINT(cert-err58-cpp)
        std::chrono::high_resolution_clock::now().time_since_epoch()).count());

auto RandomTimeSpan(size_t min, size_t max) {
    return std::chrono::milliseconds(min + gen() % (max - min));
}

enum WorkType {
    check, water
};

class Log {
private:
    size_t step_;
    std::mutex lock_;
    std::unordered_map<std::thread::id, std::vector<std::tuple<WorkType, int, int, int>>> works_;

public:
    Log() : step_(0) {
    }

    void Output(const std::string &comment) {
        lock_.lock();
        std::cout << comment << std::endl;
        lock_.unlock();
    }

    void StartWork(WorkType work_type, int flower, const std::string &comment) {
        lock_.lock();
        works_[std::this_thread::get_id()].emplace_back(work_type, step_++, 0, flower);
        std::cout << comment << std::endl;
        lock_.unlock();
    }

    void FinishWork() {
        lock_.lock();
        std::get<2>(works_[std::this_thread::get_id()].back()) = step_++;
        lock_.unlock();
    }

    void PrintDiagram() {
        lock_.lock();

        std::cout << std::endl;
        std::cout << "Легенда:" << std::endl;
        std::cout << " - Первый садовник:" << std::endl;
        std::cout << "   '#' - поливал" << std::endl;
        std::cout << "   '+' - проверял" << std::endl;
        std::cout << "   '-' - шел" << std::endl;
        std::cout << " - Второй садовник:" << std::endl;
        std::cout << "   '0' - поливал" << std::endl;
        std::cout << "   'o' - проверял" << std::endl;
        std::cout << "   '~' - шел" << std::endl;
        std::cout << " - Действия обоих параллельно над одним цветком:" << std::endl;
        std::cout << "   '@' - поливали (не должно быть!)" << std::endl;
        std::cout << "   'q' - проверяли" << std::endl;
        std::cout << "   '%' - один проверяд другой поливал" << std::endl;
        std::cout << "   '=' - шел" << std::endl;

        std::vector<std::string> diagram = std::vector<std::string>(step_, std::string(40, ' '));

        bool first = true;
        for (const auto &p : works_) {
            int from = 0;

            for (auto tuple : p.second) {
                int step = std::get<1>(tuple);

                if (first) {
                    if (from < std::get<3>(tuple)) {
                        for (int i = from; i < std::get<3>(tuple); i++) {
                            diagram[step][i] = '-';

                            if (i == std::get<3>(tuple) - 1 && diagram[step][i] == '-') {
                                diagram[step][i] = '>';
                            }
                        }
                    }
                    if (from > std::get<3>(tuple)) {
                        for (int i = std::get<3>(tuple) + 1; i <= from; i++) {
                            diagram[step][i] = '-';

                            if (i == std::get<3>(tuple) + 1 && diagram[step][i] == '-') {
                                diagram[step][i] = '<';
                            }
                        }
                    }
                    from = std::get<3>(tuple);
                    if (std::get<0>(tuple) == water) {
                        for (int i = std::get<1>(tuple); i <= std::get<2>(tuple); i++) {
                            diagram[i][from] = '#';
                        }
                    }
                    if (std::get<0>(tuple) == check) {
                        for (int i = std::get<1>(tuple); i <= std::get<2>(tuple); i++) {
                            diagram[i][from] = '+';
                        }
                    }

                } else {
                    if (from < std::get<3>(tuple)) {
                        for (int i = from; i < std::get<3>(tuple); i++) {
                            if (diagram[step][i] != '-') {
                                if (diagram[step][i] == ' ') {
                                    diagram[step][i] = '~';
                                }
                            } else {
                                diagram[step][i] = '=';
                            }

                            if (i == std::get<3>(tuple) - 1 && diagram[step][i] == '~') {
                                diagram[step][i] = '>';
                            }
                        }
                    }
                    if (from > std::get<3>(tuple)) {
                        for (int i = std::get<3>(tuple) + 1; i <= from; i++) {
                            if (diagram[step][i] != '-') {
                                if (diagram[step][i] == ' ') {
                                    diagram[step][i] = '~';
                                }
                            } else {
                                diagram[step][i] = '=';
                            }

                            if (i == std::get<3>(tuple) + 1 && diagram[step][i] == '~') {
                                diagram[step][i] = '<';
                            }
                        }
                    }
                    from = std::get<3>(tuple);
                    if (std::get<0>(tuple) == water) {
                        for (int i = std::get<1>(tuple); i <= std::get<2>(tuple); i++) {
                            if (diagram[i][from] == '+') {
                                diagram[i][from] = '%';
                            } else if (diagram[i][from] == '#') {
                                diagram[i][from] = '@';
                            } else {
                                diagram[i][from] = '0';
                            }
                        }
                    }
                    if (std::get<0>(tuple) == check) {
                        for (int i = std::get<1>(tuple); i <= std::get<2>(tuple); i++) {
                            if (diagram[i][from] == '#') {
                                diagram[i][from] = '%';
                            } else if (diagram[i][from] == '+') {
                                diagram[i][from] = 'q';
                            } else {
                                diagram[i][from] = 'o';
                            }
                        }
                    }
                }

            }
            first = false;

        }


        std::cout << std::endl << "       ";
        for (int i = 1; i <= 40; i++) {
            std::string s = std::to_string(i);
            if (s.length() > 1) {
                std::cout << s[0];
            } else {
                std::cout << " ";
            }
        }
        std::cout << std::endl << "       ";
        for (int i = 1; i <= 40; i++) {
            std::string s = std::to_string(i);
            if (s.length() > 1) {
                std::cout << s[1];
            } else {
                std::cout << s[0];
            }
        }
        std::cout << "  flower" << std::endl << "       ";
        for (int i = 1; i <= 40; i++) {
            std::cout << "-";
        }
        std::cout << "---------->" << std::endl;

        for (int i = 0; i < diagram.size(); i++) {
            std::cout << i + 1;
            std::cout << std::string(std::max(6 - (int) std::to_string(i + 1).length(), 1), ' ') << "| " << diagram[i]
                      << std::endl;
        }

        std::cout << "      |" << std::endl;
        std::cout << "steps V" << std::endl;

        lock_.unlock();
    }
};

class Flower {
private:
    bool dry = false;
    std::mutex watering_lock;
public:
    explicit Flower(bool) {
        dry = false;
        watering_lock.unlock();
    }

    Flower(Flower &&flower) noexcept: Flower(true) {
    }

    void Dry(const std::string &if_dry_msg, std::shared_ptr<Log> log) {
        watering_lock.lock();
        if (!dry) {
            dry = true;
            std::stringstream s;
            s << "Tread " << std::this_thread::get_id() << ":\t" << if_dry_msg;
            log->Output(s.str());
            std::this_thread::sleep_for(RandomTimeSpan(5, 10));
        }
        watering_lock.unlock();
    }

    void Water(int index, std::shared_ptr<Log> log) {
        watering_lock.lock();
        std::stringstream s;
        s << "Tread " << std::this_thread::get_id() << ":\tпроверяет цветок " << std::to_string(index + 1);
        log->StartWork(check, index, s.str());
        std::this_thread::sleep_for(RandomTimeSpan(1, 3));
        log->FinishWork();
        if (dry) {
            dry = false;
            std::stringstream s2;
            s2 << "Tread " << std::this_thread::get_id() << ":\tполивает цветок " << std::to_string(index + 1);
            log->StartWork(water, index, s2.str());
            std::this_thread::sleep_for(RandomTimeSpan(3, 7));
            log->FinishWork();
        }
        watering_lock.unlock();
    }
};

auto start = std::chrono::high_resolution_clock::now();

void Drought(std::shared_ptr<std::vector<Flower>> garden, std::shared_ptr<Log> log) {
    while (std::chrono::high_resolution_clock::now() - start < duration) {
        size_t flower = gen() % 40;
        (*garden)[flower].Dry("цветок " + std::to_string(flower + 1) + " засыхает", log);
    }
}

void Gardener(std::shared_ptr<std::vector<Flower>> garden, std::shared_ptr<Log> log) {
    while (std::chrono::high_resolution_clock::now() - start < duration) {
        size_t flower = gen() % 40;
        (*garden)[flower].Water(flower, log);
    }
}


int main() {
    std::cout << "Введите продолжительность работы программы, от 10 до 1000" << std::endl
              << "(в миллесекундах, помните, что цветок сохнет примерно раз в 10ms)" << std::endl;
    int t;
    t = -1;
    // M рядов по N шкафов по K книг в каждом шкафу
    while (t < 10 || t > 1000) {
        std::string s;
        std::cin.sync();
        std::getline(std::cin, s);
        std::stringstream stm(s);
        stm >> t;
        if (t < 10 || t > 1000) {
            std::cout << "Введите еще раз:" << std::endl;
        }
    }

    std::cout << "Программа будет работать ~" << t << "ms" << std::endl;
    duration = std::chrono::milliseconds (t);
    auto log = std::make_shared<Log>();

    std::shared_ptr<std::vector<Flower>> garden = std::make_shared<std::vector<Flower>>();

    for (int i = 0; i < 40; i++) {
        (*garden).emplace_back(false);
    }

    start = std::chrono::high_resolution_clock::now();
    std::thread drought(Drought, garden, log);
    std::thread gardener1(Gardener, garden, log);
    std::thread gardener2(Gardener, garden, log);

    drought.join();
    gardener1.join();
    gardener2.join();
    log->PrintDiagram();
    return 0;
}

#pragma clang diagnostic pop
